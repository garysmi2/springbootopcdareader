package org.xorcache.opc;

import java.net.UnknownHostException;
import java.util.concurrent.Executors;

import org.jinterop.dcom.common.JIException;
import org.openscada.opc.lib.common.AlreadyConnectedException;
import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.common.NotConnectedException;
import org.openscada.opc.lib.da.AccessBase;
import org.openscada.opc.lib.da.AddFailedException;
import org.openscada.opc.lib.da.DuplicateGroupException;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.ItemState;
import org.openscada.opc.lib.da.Server;
import org.openscada.opc.lib.da.SyncAccess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class OPCDAReader {

	private static Logger log = LoggerFactory
		      .getLogger(OPCDAReader.class);
	
	@Value("${opc.server.tags}")
	private String[] tags;
	@Value("${opc.server.readevery:1000}")
	private int readEvery;
	@Value("${opc.server.readfor:10000}")
	private int readFor;
	
	private final ConnectionInformation connectionInformation;	
	
	public OPCDAReader(@Value("${opc.server.host}") String host,
			           @Value("${opc.server.progid:''}") String progid,
			           @Value("${opc.server.clsid:''}") String clsid,
			           @Value("${opc.server.domain:''}") String domain,
			           @Value("${opc.server.user}") String user,
			           @Value("${opc.server.password}") String password) {
        connectionInformation = new ConnectionInformation();
        connectionInformation.setHost(host);
        connectionInformation.setDomain(domain);
        connectionInformation.setUser(user);
        connectionInformation.setPassword(password);
        connectionInformation.setProgId(progid);
        connectionInformation.setClsid(clsid);
	}
	
	private void addTags(final AccessBase accessBase) {
		for(String tag: tags) {	
			try {
				accessBase.addItem(tag, (Item item, ItemState state) -> {
					//Do something with data received
					log.info("Data Received");
			        log.info(state.toString());
				});
			} catch (AddFailedException  | JIException e) {
				e.printStackTrace();
			} 
		}
	}
	
	@Scheduled(fixedRateString="${opc.server.runevery:30000}")
	public void collectData() {
        final Server server = new Server(connectionInformation, Executors.newSingleThreadScheduledExecutor());

        try {
			server.connect();
			final AccessBase accessBase = new SyncAccess(server, readEvery);
			addTags(accessBase);
            accessBase.bind();
            Thread.sleep(readFor);
            accessBase.unbind();
            server.disconnect();
		} catch (IllegalArgumentException | UnknownHostException | JIException | NotConnectedException 
				| DuplicateGroupException | AlreadyConnectedException  e) {
			log.error(e.getMessage());
		}  catch (InterruptedException e) {
			log.error(e.getMessage());
		}

	}
	
}
