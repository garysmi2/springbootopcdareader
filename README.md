# Example Reading OpcDA Using Spring Boot

A simple spring boot application using utgard based on the example from OpenScada to connect to an OPCDA server and read some values from it.

The program will run a fixed delay scheduled task and using a list of tags defined in application.properties will read those values from the server.



## application.properties
```
Either progid or clsid is required

opc.server.progid=opc_program_id
```

```
Either progid or clid is required
As per the comments in the original example, if progid does not work then try clsid

opc.server.clsid=opc_clsid
```

```
Network address of the opc server. Required
opc.server.host=127.0.0.1
```

```
Domain of the opc server. Default: empty string

opc.server.domain=
```

```
The opc server user. Default: empty string

opc.server.user=opc_user_name
```

```
The opc server user password Default: empty string

opc.server.password=opc_user_password
```

```
How often to read values from the server in milliseconds. Default 1seconds/1000milliseconds

opc.server.readevery=1000
```

```
How long to read values from the server for in milliseconds. If readfor is 10seconds/10000ms and
readevery is 1second/1000ms then for 10seconds values will be read every 1second

opc.server.readfor=10000
```
```
A connection is made and run as a fixed delay scheduled task, how often to run the scheduled task
in milliseconds. Default 30seconds/30000milliseconds. 

So if runevery is 30seconds and readfor is 10seconds and readevery is 1seconds a scheduled task will be run every 30 seconds for 10 seconds. 

For that period of 10 seconds values will be read every 1 second.

opc.server.runevery=30000
```

```
A comma seperated list of values to read from the opc server

opc.server.tags=UC32_No_2:5:31:A,UC32_No_2:5:32:A,UC32_No_2:5:33:A,UC32_No_2:5:34:A,UC32_No_2:5:35:A,UC32_No_2:5:36:A,UC32_No_2:5:37:A,UC32_No_2:1:34:A,UC32_No_2:5:5:A
```


